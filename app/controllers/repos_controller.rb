require 'github_api_handler.rb'

class ReposController < ApplicationController
  before_action :search_props

  def index
  end

  def search
    (res, err) = GithubApiHandler.get_repositories(params[:q])
    @errors << err

    if res.present?
      res_body = JSON.parse(res.body)
      @repos += res_body['items']
      @errors << 'No repositories were found' if res_body['total_count'].zero?
    end

    render template: 'repos/index', assigns: { repos: @repos, errors: @errors }
  end

  private

  def search_props
    @errors ||= []
    @repos ||= []
  end
end
