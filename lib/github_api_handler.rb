require 'httparty'

class GithubApiHandler
  include HTTParty
  base_uri 'https://api.github.com'

  class << self
    def get_repositories(search_term)
      res = get(
        '/search/repositories',
        query: {
          q: search_term,
          sort: 'stars',
          order: 'desc',
          type: 'Repositories'
        }
      )

      case res.code
      when 200
        [ res, nil ]
      else
        [ nil, 'Unable to retrieve results' ]
      end
  rescue
      [ nil, 'Unable to retrieve results' ]
    end
  end
end
