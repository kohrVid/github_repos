require 'rails_helper'
require 'webmock/rspec'
require 'github_api_handler.rb'

RSpec.describe "GithubApiHandler" do
  describe 'get_repositories' do
    let(:search_term) { 'oct' }
    let(:status) { 200 }
    let(:api_response) { {}.to_json }

    before do
      stub_request(
        :get,
        "https://api.github.com/search/repositories?order=desc&q=#{search_term}&sort=stars&type=Repositories"
      ).with(
        headers: {
          'Accept'=>'*/*',
          'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'User-Agent'=>'Ruby'
        }
      ).to_return(status: status, body: api_response, headers: {})
    end

    subject { GithubApiHandler.get_repositories(search_term) }


    context 'when the Github API is available' do
      context 'and returns a success status code' do
        it 'returns the result with no errors' do
          expect(subject[0]).to be_an_instance_of(HTTParty::Response)
          expect(subject[1]).to be_nil
        end
      end

      context 'and does not return a success status code' do
        let(:status) { 500 }

        it 'return a nil result with an error' do
          expect(subject).to eq([nil, 'Unable to retrieve results'])
        end
      end
    end

    context 'when the Github API is not available' do
      let(:status) { 500 }

      before do
        stub_request(
          :get,
          "https://api.github.com/search/repositories?order=desc&q=#{search_term}&sort=stars&type=Repositories"
        ).with(
          headers: {
            'Accept'=>'*/*',
            'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'User-Agent'=>'Ruby'
          }
        ).to_raise(StandardError)
      end

      it 'return a nil result with an error' do
        expect(subject).to eq([nil, 'Unable to retrieve results'])
      end
    end
  end
end
