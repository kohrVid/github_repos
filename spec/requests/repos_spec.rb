require 'rails_helper'
require 'webmock/rspec'

RSpec.describe "Repos", type: :request do
  describe "GET /index" do
    it 'displays the correct page content' do
      visit repos_path
      expect(page).to have_content('Github Repos')
      expect(page).to have_content('Search repos')
    end
  end

  describe "GET /search" do
    let(:search_term) { 'oct' }
    let(:status) { 200 }
    let(:api_response) { {} }

    context 'when the Github API is available' do
      before do
        stub_request(
          :get,
          "https://api.github.com/search/repositories?q=#{search_term}&sort=stars&order=desc&type=Repositories"
        ).to_return(status: status, body: api_response.to_json)
      end

      context 'and returns a success status code' do
        context 'and there are search results' do
          let(:api_response) do
            {
              total_count: 1,
              incomplete_results: false,
              items: [
                {
                  id: 1234,
                  node_id: 'A1b2C3d4E5==',
                  name: 'oct',
                  full_name: 'octocat/oct',
                  private: false,
                  owner: {
                    login: 'octocat',
                    id: 1234,
                    node_id: 'A1b2C3d4E5'.to_json,
                    avatar_url: 'https://avatars.githubusercontent.com/u/1234',
                    html_url: 'https://github.com/octocat'
                  },
                  html_url: 'https://github.com/octocat/oct',
                  stargazers_count: 1,
                  watchers_count: 0,
                  language: 'JavaScript',
                  forks_count: 2,
                  open_issues_count: 6,
                  default_branch: 'master'
                }
              ]
            }
          end

          it 'should return a list of results' do
            visit repos_path
            fill_in 'q', with: 'oct'
            click_button 'Search repos'
            expect(page).to have_content('octocat/oct')

            expect(page).to have_xpath("//img[@src=\"https\:\/\/avatars\.githubusercontent\.com\/u\/1234\"]")
            expect(page).to have_content('0 watchers')
            expect(page).to have_content('1 star')
            expect(page).to have_content('2 forks')
            expect(page).to have_content('6 open issues')
          end
        end

        context 'and there are no results' do
          let(:api_response) do
            {
              total_count: 0,
              incomplete_results: false,
              items: []
            }
          end

          it 'should return the correct error' do
            visit repos_path
            fill_in 'q', with: 'oct'
            click_button 'Search repos'
            expect(page).to_not have_content('oct')
            expect(page).to have_content('No repositories were found')
          end
        end
      end

      context 'and does not return a success status code' do
        let(:status) { 500 }

        it 'should return the correct error' do
          visit repos_path
          fill_in 'q', with: 'oct'
          click_button 'Search repos'
          expect(page).to_not have_content('oct')
          expect(page).to have_content('Unable to retrieve results')
        end
      end
    end

    context 'when the Github API is not available' do
      let(:status) { 500 }

      before do
        stub_request(
          :get,
          "https://api.github.com/search/repositories?q=#{search_term}&sort=stars&order=desc&type=Repositories"
        ).to_raise(StandardError)
      end

      it 'should return the correct error' do
        visit repos_path
        fill_in 'q', with: 'oct'
        click_button 'Search repos'
        expect(page).to_not have_content('oct')
        expect(page).to have_content('Unable to retrieve results')
      end
    end
  end
end
