Rails.application.routes.draw do
  root 'repos#index'
  get :search, to: 'repos#search'

  resources :repos, only: [:index]
end
