# Github Repos

This is a Ruby on Rails service that is used to search for Github repos

<!-- vim-markdown-toc GFM -->

* [Prerequisites](#prerequisites)
* [Local setup](#local-setup)
* [Notes on the decision making process](#notes-on-the-decision-making-process)
  * [Architecture](#architecture)
  * [Styling](#styling)
  * [CI/CD](#cicd)

<!-- vim-markdown-toc -->

## Prerequisites

* Ruby v3+
* Bundler

## Local setup

To run the server:

    rake tmp:cache:clear && rails server

To run tests:

    rspec

To view code coverage:

    firefox coverage/index.html


## Notes on the decision making process

### Architecture

This project was originally designed as a monolithic
[SPA](https://en.wikipedia.org/wiki/Single-page_application), so as to ensure
that backend developers are able to add new features with relative ease. I do
believe however that it would benefit from decomposition, at the very least
it could be split into a backend API and a separate frontend application. In
particular the application, could not support the use of remote forms so search
results from the Github API are only displayed after the entire page has been
re-rendered.  This would perhaps not be a problem if props could be sent to a
frontend application instead.

If this application is decomposed in the future, I believe it would make sense
to create some sort of a presenter to handle the many props that are sent to
the frontend. It might also be necessary to add API documentation (e.g.,
Swagger docs) for the benefit of its consumers.

For some time I'd considered potentially caching some of the results of the
Github search in the future, in case Github's servers were ever down or slow to
respond. I concluded that such a thing would be quite complicated to set up and
so decided against this. If a decision were made to cache results in the
future, I suspect that it would also be necessary to run a Sidekiq job (or
similar) to check for any changes made in Github that correspond to data stored
on our side. Given the need to optimise for performance, a noSQL database
engine might be appropriate in this scenario (though personally, I believe the
type of data stored could lend itself well to a Postgres database).

I suspect that further work is needed on the `GithubApiHandler`, in part
because I was unsure as to how specific error handling needed to be. The case
statement used in the `.get_repositories` method should be quite
straightforward to extend (and perhaps even localise) in the future if needed.
If the app is ever expanded to allow users to search for other record types on
Github or even repositories on different hosts (e.g., Gitlab), this will need
to expand further into a service or a module. For now the class is fairly
simple as only one type of search was required.


### Styling

Originally I'd intended for the layout to resemble the cards used in one of
[Tyler Mcginnis' React tutorials](https://ui.dev/react/) however, the styling
deviated quite a bit as I worked through it and now looks completely different.
As mentioned in [a merged
commit](https://gitlab.com/kohrVid/github_repos/-/commit/4615f182034e0b903665e452179e67cafbfbeb24),
I would have liked some input from a UI/UX designer as I am not as confident in
the styling choices used here. Below are screenshots of the current user interface:

![Index page](./app/assets/images/index.png)
![Search results](./app/assets/images/search_results.png)


A finished web application should probably contain some form of navigation and
a footer. I believe it's absence here is quite jarring but as this is a
single-page application, I believe that this should be fine for now.


### CI/CD

Currently there is no CI/CD set up on this repo. With more time, I would have
liked to have set up continuous integration (though obviously, this application
doesn't need to be deployed into production just yet). I have set this
something like this up before in Github but not in Gitlab so didn't want to
spend too much time on this right now.
